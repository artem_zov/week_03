package ru.edu;

public class LinkedSimpleQueue<T> implements SimpleQueue<T> {

    /**
     * Первый элемент очереди.
     */
    private LinkedSimpleQueue.Node<T> head;

    /**
     * Последний элемент очереди.
     */

    private LinkedSimpleQueue.Node<T> tail;

    /**
     * Количество элементов в очереди.
     */

    private int size;

    /**
     * Размер очереди.
     */

    private int capacity = 0;
    LinkedSimpleQueue(final int capacityTmp) {
        this.capacity = capacityTmp;
    }

    private static class Node<T> {

        /**
         * Предыдущее значение.
         */

        private LinkedSimpleQueue.Node prev;

        /**
         * Текущее значение.
         */

        private T value;

        /**
         * Следующее значение.
         */

        private LinkedSimpleQueue.Node next;

        /**
         * @param valueTmp
         */

        Node(final T valueTmp) {
            this.value = valueTmp;
        }
    }


    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */

    @Override
    public boolean offer(final T value) {
        if (size == capacity) {
            throw new IndexOutOfBoundsException("Очередь переполнена");
        }
        Node<T> node = new Node<T>(value);
        if (size == 0) {
            head = node;
        } else {
            tail.next = node;
        }
        tail = node;
        size++;
        return true;
    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */

    @Override
    public T poll() {
        if (size == 0) {
            throw new IllegalArgumentException("Очередь пустая");
        }
        T headValue = head.value;
        head = head.next;
        size--;
        if (size == 0) {
            tail = null;
        }
        return headValue;
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */

    @Override
    public T peek() {
        if (size == 0) {
            throw new IllegalArgumentException("Пустая очередь");
        }
        return head.value;
    }


    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */

    @Override
    public int size() {
        return size;
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */

    @Override
    public int capacity() {
        return capacity - size;
    }
}


