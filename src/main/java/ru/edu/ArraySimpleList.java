package ru.edu;

public class ArraySimpleList<T> implements SimpleList<T> {

    /**
     * Определяем массив.
     *
     */

    private T[] arr;

    /**
     * Определям размер массива.
     *
     */

    private int size;

    /**
     * @param capacity - размер массива.
     *
     */

    public ArraySimpleList(final int capacity) {
        this.arr = (T[]) new Object[capacity];
    }

    /**
     *
     * Добавление элемента в конец списка.
     *
     * @param value - элемент.
     *
     */

    @Override
    public void add(final T value) {

        if (size + 1 == arr.length) {
            T[] old = arr;
            this.arr = (T[]) new Object[arr.length * 2];
            for (int i = 0; i < old.length; i++) {
                arr[i] = old[i];
            }
        }
        arr[size++] = value;
    }

    /**
     * Значения элемента по индексу.
     *
     * @param index - индекс
     * @param value - элемент
     */

    @Override
    public void set(final int index, final T value) {
        if (index > arr.length - 1 || index < 0) {
            throw new IndexOutOfBoundsException("Значение выходит "
                    + "за пределы массива");
        }
        arr[index] = value;
    }


    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */

    @Override
    public T get(final int index) {
        if (index > arr.length - 1 || index < 0) {
            return null;
        }
        return arr[index];
    }


    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */

    @Override

    public void remove(final int index) {

        if (index > arr.length - 1 || index < 0) {
            throw new IndexOutOfBoundsException("out-of-bounds index");
        }
        for (int i = index + 1; i < size; i++) {
            arr[i - 1] = arr[i];
        }
        arr[--size] = null;
    }


    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     * @return индекс элемента или -1 если не найден
     */

    @Override

    public int indexOf(final T value) {
        int i;
        for (i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */

    @Override
    public int size() {
        return size;
    }
}


