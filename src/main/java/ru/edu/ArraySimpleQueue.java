package ru.edu;


public class ArraySimpleQueue<T> implements SimpleQueue<T> {

    /**
     * Определяем массив.
     */

    private T[] arr;

    /**
     * Количество элементов в очереди.
     *
     */

    private int size = 0;

    /**
     * Размер очереди.
     */

    private int capacity = 0;

    /**
     * @param capacityParam - размер очереди.
     */

    public ArraySimpleQueue(final int capacityParam) {
        this.arr = (T[]) new Object[capacityParam];
        this.capacity = capacityParam;
    }



    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */

    @Override
    public boolean offer(final T value) {
        if (size == capacity) {
            throw new IllegalArgumentException("Очередь заполнена");
        }
        if (size + 1 > arr.length) {
            return false;
        }
        arr[size] = value;
        ++size;
        return true;
    }


    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */

    @Override
    public T poll() {
        T value;
        if (size == 0) {
            throw new IllegalArgumentException("Очередь пустая");
        }
        value = arr[0];
        for (int i = 0; i < arr.length - 1; i++) {
            arr[i + 1] = arr[i];
            if (arr[i] == null) {
                break;
            }
        }
        size--;
        return value;
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     * @return первый элемент из очереди
     */

    @Override
    public T peek() {
        if (size == 0) {
            throw new IllegalArgumentException("Пустая очередь");
        }
        return arr[0];
    }


    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */

    @Override
    public int size() {
        return size;
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */

    @Override
    public int capacity() {
        return capacity - size;
    }
}


