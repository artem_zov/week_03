package ru.edu;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;


public class ArraySimpleQueueTest extends TestCase {
    public static final String a = "a";
    public static final String b = "b";
    public static final String c = "c";
    public static final String d = "d";

    private ArraySimpleQueue<String> queue;


    @Before
    public void setUp() {
        queue = new ArraySimpleQueue<>(5);
        queue.offer(a);
        queue.offer(b);
        queue.offer(c);
    }


    @Test
    public void testOffer() {
        queue.offer(d);
        assertEquals(4, queue.size());
    }


    @Test
    public void testOfferIsFull() {
        queue.offer(d);
        queue.offer("f");
        boolean success = false;

        try {
            queue.offer("e");
        } catch (IllegalArgumentException e) {
            success = true;
        }
        assertTrue(success);
    }

    @Test
    public void testPoll() {
        queue.poll();
        assertEquals(2, queue.size());
    }


    @Test
    public void testPollIllegalArgumentException() {
        ArraySimpleQueue queueTest = new ArraySimpleQueue<>(5);
        boolean success = true;
        try{
            queueTest.poll();
        } catch (IllegalArgumentException e) {
            success = true;
        }
        assertTrue(success);
    }


    @Test
    public void testPeek() {
        assertEquals(a, queue.peek());
    }


    @Test
    public void testPeekIllegalArgumentException() {
        ArraySimpleQueue queueTest = new ArraySimpleQueue<>(5);
        boolean success = false;
        try {
            queueTest.peek();
        } catch (IllegalArgumentException e) {
            success = true;
        }
        assertTrue(success);
    }


    @Test
    public void testSize() {
        assertEquals(3, queue.size());
    }


    @Test
    public void testCapacity() {
        assertEquals(2, queue.capacity());
    }
}



