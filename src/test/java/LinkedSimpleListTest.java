package ru.edu;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

public class LinkedSimpleListTest extends TestCase {
    public static final String a = "a";
    public static final String b = "b";
    public static final String c = "c";
    public static final String d = "d";
    private SimpleList<String> list;

    @Before
    public void setUp() throws Exception {
        list = new LinkedSimpleList<>();
        list.add(a);
        list.add(b);
        list.add(c);
    }

    @Test
    public void testAdd() {
        list.add(d);
        assertEquals(d, list.get(3));
    }


    @Test
    public void testSet() {
        list.set(2, d);
        assertEquals(d, list.get(2));
    }


    @Test
    public void testSetIndexOutOfBoundsException() {
        boolean success = false;
        try {
            list.set(8, d);
        } catch (IndexOutOfBoundsException e) {
            success = true;
        }
        assertTrue(success);
    }

    @Test
    public void testGet() {
        assertEquals(a, list.get(0));
    }


    @Test
    public void testGetIndexOutOfBoundsException() {
        boolean success = false;
        try {
            list.get(8);
        } catch (IndexOutOfBoundsException e) {
            success = true;
        }
        assertTrue(success);
    }


    @Test
    public void testRemove() {
        assertEquals(3, list.size());
        list.remove(2);
        assertEquals(2, list.size());
    }


    @Test
    public void testRemoveIndexOutOfBoundsException() {
        boolean success = false;
        try {
            list.remove(8);
        } catch (IndexOutOfBoundsException e) {
            success = true;
        }
        assertTrue(success);
    }

    @Test
    public void testIndexOf() {
        assertEquals(0,  list.indexOf(a));
        assertEquals(-1, list.indexOf(d));
    }

    @Test
    public void testSize() {
        assertEquals(3, list.size());
    }
}



