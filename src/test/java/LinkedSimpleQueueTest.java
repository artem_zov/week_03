package ru.edu;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

public class LinkedSimpleQueueTest extends TestCase {
    public static final String a = "a";
    public static final String b = "b";
    public static final String c = "c";
    public static final String d = "d";

    private LinkedSimpleQueue<String> list;

    @Before
    public void setUp() {
        list = new LinkedSimpleQueue<>(5);
        list.offer(a);
        list.offer(b);
        list.offer(c);
    }


    @Test
    public void testOffer() {
        list.offer(d);
        assertEquals(4, list.size());
    }

    @Test
    public void testOfferIndexOutOfBoundsException() {
        LinkedSimpleQueue<String> listTest = new LinkedSimpleQueue<>(2);
        listTest.offer(a);
        listTest.offer(b);
        boolean success = false;
        try {
            listTest.offer(c);
        } catch (IndexOutOfBoundsException e) {
            success = true;
        }
        assertTrue(success);
    }

    @Test
    public void testPoll() {
        assertEquals(a, list.poll());
        assertEquals(2, list.size());
    }

    @Test
    public void testPollIllegalArgumentException() {
        LinkedSimpleQueue<String> listTest = new LinkedSimpleQueue<>(2);
        boolean success = false;
        try {
            listTest.poll();
        } catch (IllegalArgumentException e) {
            success = true;
        }
        assertTrue(success);
    }


    @Test
    public void testPeek() {
        assertEquals(a, list.peek());
    }

    @Test
    public void testPeekIllegalArgumentException() {
        LinkedSimpleQueue<String> listTest = new LinkedSimpleQueue<>(2);
        boolean success = false;
        try {
            listTest.peek();
        } catch (IllegalArgumentException e) {
            success = true;
        }
        assertTrue(success);
    }

    @Test
    public void testSize() {
        assertEquals(3, list.size());
    }

    @Test
    public void testCapacity() {
        assertEquals(2, list.capacity());
    }
}

