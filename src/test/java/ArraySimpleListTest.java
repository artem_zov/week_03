package ru.edu;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;


public class ArraySimpleListTest extends TestCase {

    public static final String a = "a";
    public static final String b = "b";
    public static final String c = "c";
    public static final String d = "d";

    private ArraySimpleList<String> list;


    @Before
    public void setUp() {
        list = new ArraySimpleList<>(4);
        list.add(a);
        list.add(b);
        list.add(c);
    }


    @Test
    public void testAdd() {
        list.add(b);
        assertEquals(b, list.get(1));
    }


    @Test
    public void testSet() {
        list.set(2, "c");
        assertEquals("c", list.get(2));
    }


    @Test
    public void testSetIndexOutOfBounds() {
        boolean success = false;
        try {
            list.set(8, "d");
        } catch (IndexOutOfBoundsException e) {
            success = true;
        }
        assertTrue(success);
    }


    @Test
    public void testGet() {
        assertEquals(a, list.get(0));
    }


    @Test
    public void testGetNull() {
        assertEquals(null, list.get(8));
    }


    @Test
    public void testRemove() {
        System.out.println(list.size());
        list.remove(2);
        assertEquals(2, list.size());
    }

    @Test
    public void testRemoveIndexOutOfBounds() {
        boolean success = false;
        try {
            list.remove(8);
        } catch (IndexOutOfBoundsException e) {
            success = true;
        }
        assertTrue(success);
    }


    @Test
    public void testIndexOf() {
        assertEquals(0,  list.indexOf(a));
        assertEquals(-1, list.indexOf(d));
    }

    public void testSize() {
        assertEquals(3, list.size());
    }
}



